using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class Gun_Data : ScriptableObject
{
    #region Variables
    public string gunName;
    public int maximoCount;
    public int actualAmmo;
    public float reloadTime;
    public float damage;
    public float fireRate;
    public float range;
    public Vector2 recoil;
    public Vector3 offSet;


    public bool automatic;

    public AudioClip sonidoDisparo;
    #endregion


}
