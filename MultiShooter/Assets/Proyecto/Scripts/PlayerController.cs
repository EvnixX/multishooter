using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variables

    [SerializeField] internal Camera cam;
    [SerializeField] internal Transform gunPoint;


    [SerializeField] private Transform pov;
    [SerializeField] private CharacterController controller;
    [SerializeField] private float walkSpeed = 5;
    [SerializeField] private float runSpeed = 10;
    [SerializeField] private float jumpForce = 12f;
    [SerializeField] private float gravityMod = 2.5f;

    private float horizontalRotationStore;
    private float VerticalRotationStore;
    private float actualSpeed;

    public Transform recoil;

    private Vector2 mouseInput;
    private Vector3 direccion;
    private Vector3 movement;

    [Header("Ground Detection")]
    [SerializeField] private bool isGrounded;
    [SerializeField] private float radio;
    [SerializeField] private float distance;
    [SerializeField] private Vector3 offset;
    [SerializeField] private LayerMask lm;

    public Launcher launcher;
    #endregion

    #region Unity Funcions
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        cam = Camera.main;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position + offset, radio);
        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            Gizmos.color = Color.green;
            Vector3 endPoint = ((transform.position + offset) + (Vector3.down * distance));
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);

            Gizmos.DrawSphere(hit.point, 0.1f);
        }
        else
        {
            Gizmos.color = Color.red;
            Vector3 endPoint = ((transform.position + offset) + (Vector3.down * distance));
            Gizmos.DrawWireSphere(endPoint, radio);
            Gizmos.DrawLine(transform.position + offset, endPoint);
        }
    }
    

    private void Update()
    {
        Rotation();
        Movement();
    }

    private void LateUpdate()
    {
        cam.transform.position = recoil.position;
        cam.transform.rotation = recoil.rotation;

        gunPoint.transform.position = recoil.position;
        gunPoint.transform.rotation = recoil.rotation;
    }
    #endregion

    #region Customs Funcions

    private void Rotation()
    {
        mouseInput = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        horizontalRotationStore += mouseInput.x;
        VerticalRotationStore -= mouseInput.y;

        VerticalRotationStore = Mathf.Clamp(VerticalRotationStore, -60, 60);

        transform.rotation = Quaternion.Euler(0, horizontalRotationStore, 0);
        pov.transform.localRotation = Quaternion.Euler(VerticalRotationStore, 0, 0);
    }

    private void Movement()
    {
        direccion = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        float velY = movement.y;
        movement = ((transform.forward * direccion.z) + (transform.right * direccion.x)).normalized;
        movement.y = velY;
        
        if(Input.GetButton("Fire3"))
        {
            actualSpeed = runSpeed;
        }
        else
        {
            actualSpeed = walkSpeed;
        }

        if(IsGrounded())
        {
            movement.y = 0;
        }

        if(Input.GetButtonDown("Jump") && IsGrounded())
        {
            movement.y = jumpForce * Time.deltaTime;
        }

        movement.y += Physics.gravity.y * Time.deltaTime * gravityMod;
        controller.Move(movement * (actualSpeed * Time.deltaTime));
    }  
    
    private bool IsGrounded()
    {
        isGrounded = false;

        if (Physics.SphereCast(transform.position + offset, radio, Vector3.down, out RaycastHit hit, distance, lm))
        {
            isGrounded = true;
        }
        return isGrounded;
    }
    #endregion
}
