using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    #region Variables
    public Gun_Data gun_Data;
    public Transform muzzlePoint;
    #endregion

    #region Unity Funcions
    private void Awake()
    {
        gun_Data.actualAmmo = gun_Data.maximoCount;
    }
    #endregion
}
