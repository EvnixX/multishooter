using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using Photon.Realtime;

public class Launcher : MonoBehaviourPunCallbacks
{
    #region Variables

    public static Launcher Instance;
    
    

    [SerializeField] private GameObject[] screenObjects;
    [SerializeField] private TMP_Text infoText;
    [SerializeField] private TMP_InputField roomNameInput;
    [SerializeField] TMP_Text nombreSala;
    [SerializeField] TMP_Text error;

    [SerializeField] RoomButton preButton;
    [SerializeField] Transform scrool;
    [SerializeField] private List<RoomButton> buttons = new List<RoomButton>();
    

    #endregion

    #region Funcions Unity

    private void Awake()
    {
        if( Instance == null)
        {

        Instance = this;
            
        }
        else
        {
         Destroy(this);
        }

    }


    public void Start()
    {
        infoText.text = "Cargando...";
        SetScreenObjects(0);
        PhotonNetwork.ConnectUsingSettings();
    }

    #endregion


    #region Photon

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinLobby();
        SetScreenObjects(1);
    }


    public override void OnJoinedRoom()
    {
        nombreSala.text = PhotonNetwork.CurrentRoom.Name;
        SetScreenObjects(3);
    }
    
    

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        error.text = "Ya existe una sala con este nombre." + message;
        SetScreenObjects(4);
    }

    public void ReturnMenu()
    {
        PhotonNetwork.LeaveRoom();
        SetScreenObjects(1);

    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        for (int i = 0; i < roomList.Count; i++)
        {
            if(roomList[i].RemovedFromList)
            {
                Debug.Log($"Room Name : {roomList[i].Name}");
                for (int j = 0; j < buttons.Count; j++)
                {
                    if(roomList[i].Name == buttons[j].infoRoom.Name)
                    {
                        GameObject go = buttons[j].gameObject;
                        buttons.Remove(buttons[j]);
                        Destroy(go);
                    }
                }
                
            }

        }
        
        for (int i = 0; i < roomList.Count; i++)
        {
            if (roomList[i].PlayerCount != roomList[i].MaxPlayers && !roomList[i].RemovedFromList)
            {
                RoomButton newRoomButton = Instantiate(preButton, scrool);
                newRoomButton.SetButtonDetails(roomList[i]);
                buttons.Add(newRoomButton);


               foreach(RoomButton item in buttons)
                {
                    if(roomList[i].RemovedFromList == true)
                    {
                        Destroy(newRoomButton);
                    }
                }
            }
            
            
            
        }
    }

    #endregion


    #region Custom Funcions

    public void SetScreenObjects(int index)
    {
        for (int i = 0; i < screenObjects.Length; i++)
        {
            screenObjects[i].SetActive(i == index);
        }
    }

    public void CreateRoom()
    {
        if (!string.IsNullOrEmpty(roomNameInput.text))
        {
            RoomOptions options = new RoomOptions();
            options.MaxPlayers = 8;

            infoText.text = "Creating Room";
            PhotonNetwork.CreateRoom(roomNameInput.text);

            SetScreenObjects(0);
        }
    }

    public void JoinRoom(RoomInfo info)
    {
        PhotonNetwork.JoinRoom(info.Name);
        infoText.text = "Joining Room...";
        SetScreenObjects(0);
    }
    #endregion
    


}
