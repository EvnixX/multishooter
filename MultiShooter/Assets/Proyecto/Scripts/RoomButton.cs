using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class RoomButton : MonoBehaviour
{
    #region Variables;

    [SerializeField] TMP_Text buttonText;
    public RoomInfo infoRoom;

    #endregion


    #region Customs Funcions

    #endregion
    public void SetButtonDetails(RoomInfo inputInfo)
    {
        infoRoom = inputInfo;

        buttonText.text = infoRoom.Name;

    }

    public void JoiningRoom()
    {
        Launcher.Instance.JoinRoom(infoRoom);
    }
}
