using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GunController : MonoBehaviour
{
    #region Variables
    public PlayerController playerC;
    public Gun[] guns;
    public Gun actualGun;
    public int indexGun = 0;
    public int maxGuns = 3;


    public GameObject preBullHole;
    float LastShooTime = 0;
    Vector3 currentRotation;
    Vector3 targetRotation;
    public float returnSpeed;
    public float snappines;

    public float lastReload;
    public bool reloading;
    public int actualArmor;

    float changeTime;
    float lastChangeTime;
    bool isChangin;

    public int distance;

    public VisualEffect vfx;

    public AudioClip recargar;


    #endregion

    #region Unity Funcions
    private void Update()
    {
        
        if(actualGun != null)
        {
            if(LastShooTime <= 0 && !reloading)
            {
                if (!actualGun.gun_Data.automatic)
                {
                    if (Input.GetButtonDown("Fire1"))
                    {
                        if (actualGun.gun_Data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
                else
                {
                    if (Input.GetButton("Fire1"))
                    {
                        if (actualGun.gun_Data.actualAmmo > 0)
                        {
                            Shoot();
                        }
                    }
                }
            }

            if (Input.GetButtonDown("Reload") && !reloading)
            {
                if (actualGun.gun_Data.actualAmmo < actualGun.gun_Data.maximoCount)
                {
                    lastReload = 0;
                    AudioSource.PlayClipAtPoint(recargar, gameObject.transform.position);
                    reloading = true;
                }

            }

            if( Input.GetButtonDown("Interactuar") && !reloading && !isChangin)
            {
                Drop();
            }
        }

        else
        {

        }

        if(LastShooTime >= 0)
        {
            LastShooTime -= Time.deltaTime;
        }

        if(reloading)
        {
            lastReload += Time.deltaTime;
            if(lastReload >= actualGun.gun_Data.reloadTime)
            {
                reloading = false;
                Reload();
            }
        }

        targetRotation = Vector3.Lerp(targetRotation, Vector3.zero, returnSpeed * Time.deltaTime);
        currentRotation = Vector3.Slerp(currentRotation, targetRotation, snappines * Time.deltaTime);
        playerC.recoil.localRotation = Quaternion.Euler(currentRotation);

        if(Input.GetButtonDown("Gun1") && !reloading)
        {
            if(indexGun != 0)
            {
                
                indexGun = 0;
                lastChangeTime = 0;

                if(actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }

                isChangin = true;

            }
        }


        if (Input.GetButtonDown("Gun2") && !reloading)
        {
            if (indexGun != 1)
            {
                
                indexGun = 1;
                lastChangeTime = 0;

                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }

                isChangin = true;

            }
        }


        if (Input.GetButtonDown("Gun3") && !reloading)
        {
            if (indexGun != 2)
            {
               
                indexGun = 2;
                lastChangeTime = 0;

                if (actualGun != null)
                {
                    actualGun.gameObject.SetActive(false);
                    actualGun = null;
                }

                isChangin = true;

            }
        }

        if (isChangin)
        {
            lastChangeTime += Time.deltaTime;

            if(lastChangeTime >= changeTime)
            {
                isChangin = false;
                ChangeGun(indexGun);
            }
        }

        if(Physics.Raycast(playerC.cam.transform.position, playerC.cam.transform.forward, out RaycastHit hit, distance))
        {
            if(hit.transform.tag == ("IsGun"))
            {
                if(Input.GetButtonDown("Interactuar") && !reloading && !isChangin)
                {
                    GetGun(hit.transform.GetComponent<Gun>());
                }
            }
        }
        

    }
    #endregion

    #region Custom Funcions
    private void Shoot()
    {
        if(Physics.Raycast(playerC.cam.transform.position, playerC.cam.transform.forward,out RaycastHit hit, actualGun.gun_Data.range))
        {
            if(hit.transform != null)
            {
                Debug.Log($"We shootin at:{hit.transform.name}");
                Instantiate(preBullHole, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal, Vector3.up));
                GameObject go = Instantiate(preBullHole, hit.point + hit.normal * 0.01f, Quaternion.LookRotation(hit.normal, Vector3.up));

                Destroy(go, 1.5f);
               
            }
        }
        actualGun.gun_Data.actualAmmo--;
        LastShooTime = actualGun.gun_Data.fireRate;

        Addrecoil();
        GameObject efect = VisualEffect.Instantiate(vfx, actualGun.muzzlePoint.position, actualGun.muzzlePoint.rotation).gameObject;
        efect.transform.parent = actualGun.muzzlePoint;
        Destroy(efect, 1.5f);
        AudioSource.PlayClipAtPoint(actualGun.gun_Data.sonidoDisparo, gameObject.transform.position);
        
    }

    void Addrecoil()
    {
        targetRotation += new Vector3(-actualGun.gun_Data.recoil.x,Random.Range(-actualGun.gun_Data.recoil.y, actualGun.gun_Data.recoil.y),0f);
    }

    private void Reload()
    {
        actualGun.gun_Data.actualAmmo = actualGun.gun_Data.maximoCount;
    }

    void ChangeGun(int index)
    {
        if(guns[index] != null)
        {
            actualGun = guns[index];
            actualGun.gameObject.SetActive(true);  
        }
    }

    void Drop()
    {
        actualGun.GetComponent<Rigidbody>().useGravity = true;
        actualGun.GetComponent<Rigidbody>().isKinematic = false;

        actualGun.gameObject.transform.SetParent(null);
        actualGun = null;

        guns[indexGun] = null;

    }
    
    void GetGun(Gun getGun)
    {
        if( actualGun != null)
        {
            Drop();
        }

        actualGun = getGun;

        actualGun.GetComponent<Rigidbody>().useGravity = false;
        actualGun.GetComponent<Rigidbody>().isKinematic = true;

        actualGun.transform.parent = playerC.gunPoint;
        actualGun.transform.localPosition = actualGun.gun_Data.offSet;
        actualGun.transform.localRotation = Quaternion.identity;

        guns[indexGun] = actualGun;
    }
    #endregion
}
